﻿-- Ejercicio 1. Mostrar todos los campos y todos los registros de la tabla empleado.

  SELECT * FROM emple;

-- Ejercicio 2. Mostrar todos los campos y todos los registros de la tabla departamento.
  
  SELECT * FROM depart;

-- Ejercicio 3. Mostrar el número, nombre y localización de cada departamento.

  SELECT dept_no, dnombre, loc FROM depart;

  SELECT * FROM depart;

-- Ejercicio 4. Datos de los empleados ordenados por número de departamento descendentemente.

  SELECT * FROM emple
  ORDER BY dept_no DESC;

-- Ejercicio 5. Datos de los empleados ordenados por número de departamento descendentemente y por oficio ascendente.

  SELECT * FROM emple
  ORDER BY dept_no DESC ,oficio asc;

-- Ejercicio 6. Datos de los empleados ordenados por número de departamento descendentemente y por apellido ascendentemente.
  
  SELECT * FROM emple
  ORDER BY dept_no DESC, apellido;   

-- Ejercicio 7. Mostrar el apellido y oficio de cada empleado.

  SELECT apellido, oficio FROM emple;

-- Ejercicio 8. Mostrar localización y número de cada departamento.

  SELECT loc,dept_no FROM depart;

-- Ejercicio 9. Datos de los empleados ordenados por apellido de forma ascendente.

  SELECT * FROM emple
  ORDER BY apellido ASC;

-- Ejercicio 10. Datos de los empleados ordenados por apellido de forma descendente.

  SELECT * FROM emple
  ORDER BY apellido DESC;

-- Ejercicio 11. Mostrar los datos de los empleados cuyo oficio sea ANALISTA.

  SELECT * FROM emple
  WHERE oficio = 'analista';

-- Ejercicio 12. Mostrar los datos de los empleados cuyo oficio se ANALISTA y ganen más de 2000
  
  SELECT * FROM emple
  WHERE oficio = 'analista' AND salario>2000;   

-- Ejercicio 13. Listar el apellido de todos los empleados y ordenarlos por oficio y por nombre

  SELECT apellido FROM emple
  ORDER BY oficio, apellido;

-- Ejercicio 14. Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por Z. Listar todos los campos de la tabla empleados.

  SELECT * FROM emple
  WHERE apellido <> 'Sánchez' AND apellido <> 'fernandez' AND apellido <> 'Jimenez' AND apellido <> 'Muñoz' AND oficio = 'empleado';


  SELECT * FROM emple WHERE apellido NOT LIKE '%z';
  
-- Ejercicio 15. Mostrar el código de los empleados cuyo salario sea mayor que 2000.

  SELECT emp_no FROM emple
  WHERE salario > 2000;

-- Ejercicio 16. Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000.

  SELECT emp_no, apellido FROM emple
  WHERE salario < 2000;

-- Ejercicio 17. Mostrar los datos de los empleados cuyo salario esté entre 1500 y 2500.

  SELECT * FROM emple
  WHERE salario BETWEEN 1500 AND 2500;

-- Ejercicio 18. Seleccionar el apellido y oficio de los empleados del departamento número 20.

  SELECT apellido, oficio FROM emple
  WHERE dept_no = '20';

-- Ejercicio 19. Mostrar todos los datos de empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente.

  SELECT * FROM emple
  WHERE apellido LIKE 'm%' OR apellido LIKE 'n%'
  ORDER BY apellido;

-- Ejercicio 20. Seleccionar los datos de los empleados cuyo oficio sea VENDEDOR. Mostrar los datos ordenados por apellido de forma ascendente.

  SELECT * FROM emple
  WHERE oficio = 'vendedor'
  ORDER BY apellido;

-- Ejercicio 21. Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ANALISTA. Ordenar el resultado por apellido y oficio de forma ascendente.

SELECT * FROM emple
WHERE dept_no = 10 AND oficio = 'ANALISTA'
ORDER BY apellido , oficio;

-- Ejercicio 22. Realizar un listado de los distintos meses en que los empleados se han dado de alta.

SELECT distinct MONTH(fecha_alt) FROM emple;

-- Ejercicio 23. Realizar un listado de los distintos años en los que los empleados se han dado de alta.

SELECT DISTINCT year(fecha_alt) FROM emple;

-- Ejercicio 24. Realizar un listado de los distintos días del mes en que los empleados se han dado de alta.

SELECT DISTINCT DAY(fecha_alt) FROM emple;

-- Ejercicio 25. Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento número 20.

SELECT apellido FROM emple
WHERE salario>2000 OR dept_no=20;

-- Ejercicio 26. Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A o por M. Listar el apellido de los empleados.

SELECT apellido FROM emple
WHERE apellido LIKE 'a%' OR apellido LIKE 'm%' ;

-- Ejercicio 27. Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A. Listar el apellido de los empleados.

SELECT apellido FROM emple
WHERE apellido LIKE 'a%';

SELECT apellido FROM emple
WHERE substr(apellido,1,1)='a';

-- Ejercicio 28. Seleccionar de la tabla EMPLE aquellas filas cuyo apellido empiece por A y el oficio tenga una E en cualquier posición. Ordenar la salida por oficio y por salario de forma descendente.

SELECT * FROM emple
WHERE apellido LIKE 'a%' and oficio LIKE '%e%'
ORDER BY oficio, salario desc;

-- Ejercicio 29. Indicar el número de empleados que hay.

SELECT COUNT(emp_no) FROM emple;

-- Ejercicio 30. Indicar el número de departamentos que hay.

SELECT COUNT(distinct dept_no) FROM depart;
SELECT * FROM depart;

-- Ejercicio 31. Contar el número de empleados cuyo oficio sea vendedor.

SELECT COUNT(*) FROM emple
WHERE oficio = 'vendedor';

-- Ejercicio 32. Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece. Ordena el resultado por apellido.

SELECT apellido, dept_no FROM emple
ORDER BY apellido;


SELECT apellido,dnombre FROM emple JOIN depart USING(dept_no)
ORDER BY apellido;

-- Ejercicio 33. Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del departamento al que pertenece. Ordenar los resultados por apellido de forma descendente.

SELECT apellido,oficio,dnombre FROM emple JOIN depart USING(dept_no)
ORDER BY apellido DESC;

-- Ejercicio 34. Mostrar los apellidos del empleado que más gana.

SELECT apellido FROM emple
WHERE salario = (SELECT MAX(salario) FROM emple);

-- Ejercicio 35. Indicar el número de empleados más el número de departamentos.

SELECT (SELECT COUNT(*) FROM emple)+
(SELECT COUNT(*) FROM depart);

-- Ejercicio 36. Listar el número de departamento de aquellos departamentos que tengan empleados y el número de empleados que tengan.

SELECT dept_no,COUNT(*) FROM emple GROUP BY dept_no;